﻿using WebGoatCore.Models;
using WebGoatCore.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Net.Http;
using WebGoat.NET.ModelsPrimitives;
using WebGoat.NET.ModelsDTO;

namespace WebGoatCore.Controllers
{
    [Route("[controller]/[action]")]
    public class BlogController : Controller
    {
        private readonly BlogEntryRepository _blogEntryRepository;
        private readonly BlogResponseRepository _blogResponseRepository;

        public BlogController(BlogEntryRepository blogEntryRepository, BlogResponseRepository blogResponseRepository, NorthwindContext context)
        {
            _blogEntryRepository = blogEntryRepository;
            _blogResponseRepository = blogResponseRepository;
        }

        public IActionResult Index()
        {
            return View(_blogEntryRepository.GetTopBlogEntries());
        }

        [HttpGet("{entryId}")]
        public IActionResult Reply(int entryId)
        {
            return View(_blogEntryRepository.GetBlogEntry(entryId));
        }

        [HttpPost("{entryId}")]
        [ValidateAntiForgeryToken]
        public IActionResult Reply(BlogResponseDTO br_DTO)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            var author = User?.Identity?.Name ?? "Anonymous";
            try
            {
                ContentsPrimitive contentsValidated = new ContentsPrimitive(br_DTO.Contents);           //Validate contents
                BlogEntryIdPrimitive blogEntryIdValidated = new BlogEntryIdPrimitive(br_DTO.EntryId);   //Validate BlogEntryId
                AuthorPrimitive authorValidated = new AuthorPrimitive(author);                          //Validate Author
                CleanBlogResponse response = new CleanBlogResponse(blogEntryIdValidated, contentsValidated, authorValidated, DateTime.Now);

                var toDB = new BlogResponse(response.BlogEntryId.CleanBlogEntryId, response.Contents.CleanContents, 
                    response.Author.CleanAuthor, response.ResponseDate);
                
                _blogResponseRepository.CreateBlogResponse(toDB);
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("InvalidContents", ex.Message.ToString());
                return View(_blogEntryRepository.GetBlogEntry(br_DTO.EntryId));
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult Create() => View();

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult Create(string title, string contents)
        {
            var blogEntry = _blogEntryRepository.CreateBlogEntry(title, contents, User!.Identity!.Name!);
            return View(blogEntry);
        }

    }
}