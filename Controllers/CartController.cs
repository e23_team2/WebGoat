﻿using WebGoatCore.Models;
using WebGoatCore.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using WebGoat.NET.ModelsDTO;
using WebGoat.NET.ModelsPrimitives;
using WebGoatCore.ViewModels;

namespace WebGoatCore.Controllers
{
    [Route("[controller]/[action]")]
    public class CartController : Controller
    {
        private readonly ProductRepository _productRepository;

        public CartController(ProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        private Cart GetCart()
        {
            HttpContext.Session.TryGet<Cart>("Cart", out var cart);
            if(cart == null)
            {
                cart = new Cart();
            }
            return cart;
        }

        public IActionResult Index()
        {
            return View(GetCart());
        }

        [HttpPost("{productId}")]
        public IActionResult AddOrder(OrderDetailDTO OrderDetail)
        {

            if (!ModelState.IsValid)
            {
                return RedirectToAction("Details", "Product", new { productId = OrderDetail.ProductId, quantity = OrderDetail.Quantity });
            }

            var product = _productRepository.GetProductById(OrderDetail.ProductId);
            
            var cart = GetCart();
            if(!cart.OrderDetails.ContainsKey(OrderDetail.ProductId))
            {
                try
                {
                    QuantityPrimitive quantityValidated = new QuantityPrimitive(OrderDetail.Quantity);  //validate quantity 

                    var orderDetailInput = new OrderDetailInput(OrderDetail.ProductId, product.UnitPrice, quantityValidated, 0.0F, product);

                    var orderDetail = new OrderDetail(orderDetailInput.ProductId, orderDetailInput.UnitPrice, orderDetailInput.Quantity.Quantity, orderDetailInput.Discount)
                    {
                        Product = product
                    };

                    cart.OrderDetails.Add(orderDetail.ProductId, orderDetail);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("InvalidOrderDetail", ex.Message.ToString());
                    return RedirectToAction("Details", "Product", new { productId = OrderDetail.ProductId, quantity = OrderDetail.Quantity });
                }
                
            }
            else
            {
                var originalOrder = cart.OrderDetails[OrderDetail.ProductId];
                originalOrder.Quantity += OrderDetail.Quantity;        
            }

            HttpContext.Session.Set("Cart", cart);
            return RedirectToAction("Index");
        }

        [HttpGet("{productId}")]
        public IActionResult RemoveOrder(int productId)
        {
            try
            {
                var cart = GetCart();
                if (!cart.OrderDetails.ContainsKey(productId))
                {
                    return View("RemoveOrderError", string.Format("Product {0} was not found in your cart.", productId));
                }

                cart.OrderDetails.Remove(productId);
                HttpContext.Session.Set("Cart", cart);

                Response.Redirect("~/ViewCart.aspx");
            }
            catch (Exception ex)
            {
                return View("RemoveOrderError", string.Format("An error has occurred: {0}", ex.Message));
            }

            return RedirectToAction("Index");
        }
    }
}