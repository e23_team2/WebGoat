﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WebGoat.NET.ModelsDTO
{
    public class BlogResponseDTO
    {
        [NotNull, Range(0, int.MaxValue)]
        public int EntryId { get; set; }
        [NotNull, MinLength(1), MaxLength(256)]
        public string Contents { get; set; }
    }
}
