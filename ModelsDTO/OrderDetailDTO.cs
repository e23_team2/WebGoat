﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace WebGoat.NET.ModelsDTO
{
    public class OrderDetailDTO
    {
        [NotNull]
        public int ProductId { get; set; }

        [NotNull, Range(1,200)]
        public short Quantity { get; set; }
    }
}
