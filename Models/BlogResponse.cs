﻿using System;
using System.Text.Encodings.Web;
using System.Web;
using System.Configuration;
using Microsoft.AspNetCore.Mvc;
using WebGoat.NET.ModelsPrimitives;
using Microsoft.Build.Framework;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
namespace WebGoatCore.Models
{
    public class BlogResponse
    {
        public int Id { get; private set; }
        public int BlogEntryId { get; private set; }
        public DateTime ResponseDate { get; private set; }
        public string Author { get; private set; }
        public string Contents { get; private set; }

        public virtual BlogEntry BlogEntry { get; set; }

        public BlogResponse(int blogEntryId, string contents, string author, DateTime responseDate) 
        {
            BlogEntryId = blogEntryId;
            ResponseDate = responseDate;
            Author = author;
            Contents = contents;
        }
    }

    public class CleanBlogResponse
    {
        public BlogEntryIdPrimitive BlogEntryId { get; private set; }
        public DateTime ResponseDate { get; private set; }
        public AuthorPrimitive Author { get; private set; }
        public ContentsPrimitive Contents { get; private set; }
        
        public CleanBlogResponse(BlogEntryIdPrimitive blogEntryId, 
            ContentsPrimitive contents, AuthorPrimitive author, DateTime responseDate)
        {
            BlogEntryId = blogEntryId;
            ResponseDate = responseDate;
            Author = author;
            Contents = contents;
        }
    }
}
