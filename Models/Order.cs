﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Globalization;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using static System.Runtime.InteropServices.JavaScript.JSType;
using Castle.Core.Resource;
using WebGoatCore.Data;

#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
namespace WebGoatCore.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public string CustomerId { get; set; }
        public int? EmployeeId { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime RequiredDate { get; set; }
        public DateTime? ShippedDate { get; set; }
        public int ShipVia { get; set; }
        public decimal Freight { get; set; }
        public string? ShipName { get; set; }
        public string? ShipAddress { get; set; }
        public string? ShipCity { get; set; }
        public string? ShipRegion { get; set; }
        public string? ShipPostalCode { get; set; }
        public string? ShipCountry { get; set; }

        public virtual IList<OrderDetail> OrderDetails { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual IList<OrderPayment> OrderPayments { get; set; }
        public virtual Shipment? Shipment { get; set; }

        public decimal SubTotal => OrderDetails.Sum(od => od.ExtendedPrice);

        public decimal Total => Math.Round(SubTotal + Freight, 2);

        public static string GetPackageTrackingUrl(string Carrier, string TrackingNumber)
        {
            string trackingUrl;
            Carrier = Carrier.ToLower();
            switch (Carrier)
            {
                case "fedex":
                case "federalexpress":
                case "federal express":
                    trackingUrl = string.Format("http://www.fedex.com/Tracking?tracknumbers={0}&action=track", TrackingNumber);
                    break;
                case "ups":
                case "unitedpostalservice":
                case "united postal service":
                    //trackingUrl = string.Format("http://wwwapps.ups.com/WebTracking/processInputRequest?InquiryNumber1={0}&tracknums_displayed=1&TypeOfInquiryNumber=T", TrackingNumber);
                    trackingUrl = string.Format("http://wwwapps.ups.com/WebTracking/track?loc=en_US&track.x=Track&trackNums={0}", TrackingNumber);
                    break;
                case "usps":
                case "unitedstatespostalservice":
                case "united states postal service":
                case "postoffice":
                case "post office":
                    trackingUrl = string.Format("http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?origTrackNum={0}", TrackingNumber);
                    break;
                default:
                    trackingUrl = string.Format("http://{0}?TrackingNumber={1}", Carrier, TrackingNumber);
                    break;
            }
            return trackingUrl;
        }
    }

    public class PostalCodePrimitive
    {
        public string PostalCode { get; private set; }

        public PostalCodePrimitive(string cleanPostalCode) 
        {
            IsPostalCodeValid(cleanPostalCode);
            PostalCode = cleanPostalCode;
        }

        private void IsPostalCodeValid(string postalCode)
        {
            try
            {
                var regex = "^\\d{4,6}$";
                var match = Regex.Match(postalCode, regex);
                if (!match.Success)
                {
                    throw new ArgumentException("Invalid Postal Code");
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Invalid Postal Code");
            }
        }
    }

    public class CountryPrimitive
    {
        public string CleanCountry { get; private set; }

        public CountryPrimitive(string cleanCountry)
        {
            IsCountryValid(cleanCountry);
            CleanCountry = cleanCountry;
        }

        private void IsCountryValid(string country)
        {
            bool check = false;
            foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                var ri = new RegionInfo(ci.Name);
                if(ri.DisplayName.Equals(country))
                {
                    check = true;
                    break;
                }
            }
            if (!check)
            {
                throw new ArgumentException("Invalid Country");
            }
        }
    }

    public class ShippingMethodPrimitive
    {
        public int CleanShippingMethod { get; private set; }

        public ShippingMethodPrimitive(int cleanShippingMethod)
        {
            IsShippingMethodValid(cleanShippingMethod);
            CleanShippingMethod = cleanShippingMethod;
        }

        private void IsShippingMethodValid(int shippingMethod)
        {
            IntegerValidator validator = new IntegerValidator(1, 7);
            validator.Validate(shippingMethod);
        }
    }

    public class CreditCardNumberPrimitive
    {
        public string CleanCreditCardNumber { get; private set; }

        public CreditCardNumberPrimitive(string cleanCreditCardNumber)
        {
            IsCreditCardNumberValid(cleanCreditCardNumber);
            CleanCreditCardNumber = cleanCreditCardNumber;
        }

        private void IsCreditCardNumberValid(string creditCardNumber)
        {
            try
            {
                var ccNTrim = Regex.Replace(creditCardNumber, @"\s+", ""); //Remove whitespaces

                //Check if input is 13-16 digits
                var regex = "^\\d{13,16}$";
                var match = Regex.Match(ccNTrim, regex);
                if (!match.Success)
                {
                    throw new ArgumentException("Invalid Credit Card Number");
                }

                var number = ccNTrim.ToCharArray();

                // Validate based on card type, first if tests length, second tests prefix
                // Use Luhn Algorithm to validate
                int sum = 0;
                for (int i = number.Length - 1; i >= 0; i--)
                {
                    if (i % 2 == number.Length % 2)
                    {
                        int n = (number[i] - '0') * 2;
                        sum += (n / 10) + (n % 10);
                    }
                    else
                    {
                        sum += (number[i] - '0');
                    }
                }
                if(!(sum % 10 == 0))
                {
                    throw new ArgumentException("Invalid CreditCard Number");
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Invalid CreditCard Number");
            }
        }
    }

    public class CreditCardExpMonthPrimitive
    {
        public int CleanCreditCardExpMonth { get; private set; }

        public CreditCardExpMonthPrimitive(int cleanCreditCardExpMonth)
        {
            IsCreditCardExpMonthValid(cleanCreditCardExpMonth);
            CleanCreditCardExpMonth = cleanCreditCardExpMonth;
        }

        private void IsCreditCardExpMonthValid(int creditCardExpMonth)
        {
            IntegerValidator validator = new IntegerValidator(1, 12);
            validator.Validate(creditCardExpMonth);
        }
    }

    public class CreditCardExpYearPrimitive
    {
        public int CleanCreditCardYear{ get; private set; }

        public CreditCardExpYearPrimitive(int cleanCreditCardYear)
        {
            IsCreditCardYearValid(cleanCreditCardYear);
            CleanCreditCardYear = cleanCreditCardYear;
        }

        private void IsCreditCardYearValid(int creditCardYear)
        {
            IntegerValidator validator = new IntegerValidator(DateTime.Now.Year, DateTime.Now.Year + 10);
            validator.Validate(creditCardYear);
        }
    }

    public class CleanCreditCard
    {
        public CreditCardNumberPrimitive CleanCreditCardNumber { get; private set; }
        public CreditCardExpMonthPrimitive CleanCreditCardMonth { get; private set; }
        public CreditCardExpYearPrimitive CleanCreditCardYear { get; private set; }
        public DateTime CleanExpireDate { get; private set; }

        public CleanCreditCard(CreditCardNumberPrimitive cleanCreditCardNumber, CreditCardExpMonthPrimitive cleanCreditCardMonth, CreditCardExpYearPrimitive cleanCreditCardYear)
        {
            IsCreditCardDateValid(cleanCreditCardMonth.CleanCreditCardExpMonth, cleanCreditCardYear.CleanCreditCardYear);
            CleanCreditCardNumber = cleanCreditCardNumber;
            CleanCreditCardMonth = cleanCreditCardMonth;
            CleanCreditCardYear = cleanCreditCardYear;
            CleanExpireDate = new DateTime(cleanCreditCardYear.CleanCreditCardYear, cleanCreditCardMonth.CleanCreditCardExpMonth, 1);
        }

        private void IsCreditCardDateValid(int cleanCreditCardMonth, int cleanCreditCardYear)
        {
            var creditCardExpDate = new DateTime(cleanCreditCardYear, cleanCreditCardMonth, 1);
            if((creditCardExpDate - DateTime.Now).Days < 0)
            {
                throw new ArgumentException("Your Credit Card has expired!");
            }
        }
    }

    public class ShippingTargetPrimitive
    {
        public string CleanShippingTarget { get; private set; }

        public ShippingTargetPrimitive(string cleanShippingTarget)
        {
            IsShippingTargetValid(cleanShippingTarget);
            CleanShippingTarget = cleanShippingTarget;
        }

        private void IsShippingTargetValid(string shippingTarget)
        {
            StringValidator validator = new StringValidator(1, 256);
            validator.Validate(shippingTarget);
        }
    }

    public class ShippingAddressPrimitive
    {
        public string CleanShippingAddress { get; private set; }

        public ShippingAddressPrimitive(string cleanShippingAddress)
        {
            IsShippingAddressValid(cleanShippingAddress);
            CleanShippingAddress = cleanShippingAddress;
        }

        private void IsShippingAddressValid(string shippingAddress)
        {
            StringValidator validator = new StringValidator(1, 256);
            validator.Validate(shippingAddress);
        }
    }

    public class ShippingCityPrimitive
    {
        public string CleanShippingCity { get; private set; }

        public ShippingCityPrimitive(string cleanShippingCity)
        {
            IsShippingCityValid(cleanShippingCity);
            CleanShippingCity = cleanShippingCity;
        }

        private void IsShippingCityValid(string shippingCity)
        {
            StringValidator validator = new StringValidator(1, 256);
            validator.Validate(shippingCity);
        }
    }

    public class ShippingRegionPrimitive
    {
        public string CleanShippingRegion { get; private set; }

        public ShippingRegionPrimitive(string cleanShippingRegion)
        {
            IsShippingRegionValid(cleanShippingRegion);
            CleanShippingRegion = cleanShippingRegion;
        }

        private void IsShippingRegionValid(string shippingRegion)
        {
            StringValidator validator = new StringValidator(0, 256);
            validator.Validate(shippingRegion);
        }
    }

    public class CleanOrder
    {
        public CleanOrder(int employeeId, 
            DateTime orderDate, DateTime requiredDate, decimal freight,
            ShippingMethodPrimitive shipMethod, ShippingTargetPrimitive shipName, ShippingAddressPrimitive shipAddress, ShippingCityPrimitive shipCity,
            ShippingRegionPrimitive shipRegion, PostalCodePrimitive shipPostalCode, CountryPrimitive shipCountry, IList<OrderDetail> orderDetails,
            Customer customer, CleanShipment shipment)
        {
            EmployeeId = employeeId;
            OrderDate = orderDate;
            RequiredDate = requiredDate;
            Freight = freight;
            ShipVia = shipMethod;
            ShipName = shipName;
            ShipAddress = shipAddress;
            ShipCity = shipCity;
            ShipRegion = shipRegion;
            ShipPostalCode = shipPostalCode;
            ShipCountry = shipCountry;
            OrderDetails = orderDetails;
            Customer = customer;
            Shipment = shipment;
        }

        public int EmployeeId { get; private set; }
        public DateTime OrderDate { get; private set; }
        public DateTime RequiredDate { get; private set; }
        public decimal Freight { get; private set; }
        public ShippingMethodPrimitive ShipVia { get; private set; }
        public ShippingTargetPrimitive ShipName { get; private set; }
        public ShippingAddressPrimitive ShipAddress { get; private set; }
        public ShippingCityPrimitive ShipCity { get; private set; }
        public ShippingRegionPrimitive ShipRegion { get; private set; }
        public PostalCodePrimitive ShipPostalCode { get; private set; }
        public CountryPrimitive ShipCountry { get; private set; }
        public IList<OrderDetail> OrderDetails { get; private set; }
        public Customer Customer { get; private set; }
        public CleanShipment Shipment { get; private set; }
        public decimal SubTotal => OrderDetails.Sum(od => od.ExtendedPrice);
        public decimal Total => Math.Round(SubTotal + Freight, 2);

        public static string GetPackageTrackingUrl(string Carrier, string TrackingNumber)
        {
            string trackingUrl;
            Carrier = Carrier.ToLower();
            switch (Carrier)
            {
                case "fedex":
                case "federalexpress":
                case "federal express":
                    trackingUrl = string.Format("http://www.fedex.com/Tracking?tracknumbers={0}&action=track", TrackingNumber);
                    break;
                case "ups":
                case "unitedpostalservice":
                case "united postal service":
                    //trackingUrl = string.Format("http://wwwapps.ups.com/WebTracking/processInputRequest?InquiryNumber1={0}&tracknums_displayed=1&TypeOfInquiryNumber=T", TrackingNumber);
                    trackingUrl = string.Format("http://wwwapps.ups.com/WebTracking/track?loc=en_US&track.x=Track&trackNums={0}", TrackingNumber);
                    break;
                case "usps":
                case "unitedstatespostalservice":
                case "united states postal service":
                case "postoffice":
                case "post office":
                    trackingUrl = string.Format("http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?origTrackNum={0}", TrackingNumber);
                    break;
                default:
                    trackingUrl = string.Format("http://{0}?TrackingNumber={1}", Carrier, TrackingNumber);
                    break;
            }
            return trackingUrl;
        }
    }
}
