﻿using System;

#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
namespace WebGoatCore.Models
{
    public class Shipment
    {
        public int ShipmentId { get; set; }
        public int OrderId { get; set; }
        public int ShipperId { get; set; }
        public DateTime ShipmentDate { get; set; }
        public string TrackingNumber { get; set; }

        public virtual Order Order { get; set; }
        public virtual Shipper Shipper { get; set; }
    }

    public class CleanShipment
    {
        public int OrderId { get; private set; }
        public ShippingMethodPrimitive ShipperId { get; private set; }
        public DateTime ShipmentDate { get; private set; }
        public string TrackingNumber { get; private set; }

        public CleanShipment(DateTime shipmentDate, ShippingMethodPrimitive shipperId, string trackingNumber)
        {
            ShipmentDate = shipmentDate;
            ShipperId = shipperId;
            TrackingNumber = trackingNumber;
        }
    }
}
