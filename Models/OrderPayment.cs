﻿using System;
using System.Text.RegularExpressions;

#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
namespace WebGoatCore.Models
{
    public class OrderPayment
    {
        public int OrderPaymentId { get; set; }
        public int OrderId { get; set; }
        public string CreditCardNumber { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string? CVV { get; set; }
        public double AmountPaid { get; set; }
        public DateTime PaymentDate { get; set; }
        public string ApprovalCode { get; set; }

        public virtual Order Order { get; set; }
    }

    public class CleanOrderPayment
    {
        public int CleanOrderId { get; private set; }
        public string? CVV { get; private set; }
        public DateTime PaymentDate { get; private set; }
        public string ApprovalCode { get; private set; }
        public CleanCreditCard CleanCreditCard { get; private set; }
        public CleanOrder CleanOrder { get; private set; }
        
        public CleanOrderPayment(int cleanOrderId, DateTime paymentDate, 
                                 string approvalCode, CleanCreditCard cleanCreditCard, CleanOrder cleanOrder) 
        {
            IsInputValid(cleanOrderId, paymentDate, approvalCode);
            CleanOrderId = cleanOrderId;
            PaymentDate = paymentDate;
            ApprovalCode = approvalCode;
            CleanCreditCard = cleanCreditCard;
            CleanOrder = cleanOrder;
        }

        private void IsInputValid(int orderId, DateTime paymentDate, string approvalCode)
        {
            if(orderId < 0 )
            {
                throw new ArgumentException("Invalid Order Id");
            }

            if((paymentDate - DateTime.Now).Days < 0)
            {
                throw new ArgumentException("Invalid Payment Date");
            }

            var regex = "^\\d{6}$";
            var match = Regex.Match(approvalCode, regex);
            if (!match.Success)
            {
                throw new ArgumentException("Invalid Approval Code");
            }
        }
    }
}
