﻿using System;
using System.ComponentModel.DataAnnotations;
using WebGoat.NET.ModelsPrimitives;

#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
namespace WebGoatCore.Models
{
    public class OrderDetailInput
    {
        public int OrderId { get; private set; }
        public int ProductId { get; private set; }
        public double UnitPrice { get; private set; }

        public QuantityPrimitive Quantity { get; private set; }
        public float Discount { get; private set; }

        public virtual Order Order { get; private set; }
        public virtual Product Product { get; private set; }

        public decimal DecimalUnitPrice => Convert.ToDecimal(this.UnitPrice);
        public decimal ExtendedPrice => DecimalUnitPrice * Convert.ToDecimal(1 - Discount) * Quantity.Quantity;

        public OrderDetailInput(int productId, double unitPrice, QuantityPrimitive quantity, float discount, Product product)
        {
            ProductId = productId;
            UnitPrice = unitPrice;
            Quantity = quantity;
            Discount = discount;
            Product = product;
        }
    }

    public class OrderDetail
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public double UnitPrice { get; set; }

        public short Quantity { get; set; }
        public float Discount { get; set; }

        public virtual Order Order { get; set; }
        public virtual Product Product { get; set; }

        public decimal DecimalUnitPrice => Convert.ToDecimal(this.UnitPrice);
        public decimal ExtendedPrice => DecimalUnitPrice * Convert.ToDecimal(1 - Discount) * Quantity;

        public OrderDetail(int productId, double unitPrice, short quantity, float discount)
        {
            ProductId = productId;
            UnitPrice = unitPrice;
            Quantity = quantity;
            Discount = discount;
        }
    }
}
