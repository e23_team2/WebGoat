﻿using Microsoft.Extensions.Options;
using System;
using System.Configuration;
using System.Text.RegularExpressions;
using WebGoatCore;

namespace WebGoat.NET.ModelsPrimitives
{
    public class AuthorPrimitive
    {
        public string CleanAuthor { get; private set; }
        public AuthorPrimitive(string cleanAuthor)
        {
            IsAuthorValid(cleanAuthor);
            CleanAuthor = cleanAuthor;
        }
        private void IsAuthorValid(string cleanAuthor)
        {
            var regex = "^[a-zA-Z0-9-._@+<>/!]{1,256}$";
            var match = Regex.Match(cleanAuthor, regex);
            if (!match.Success)
            {
                throw new ArgumentException("Invalid Author");
            }
        }
    }
}
