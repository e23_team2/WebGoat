﻿using System.Configuration;
using System;

namespace WebGoat.NET.ModelsPrimitives
{
    public class BlogEntryIdPrimitive
    {
        public int CleanBlogEntryId { get; private set; }
        public BlogEntryIdPrimitive(int cleanBlogEntryId)
        {
            IsBlogEntryIdValid(cleanBlogEntryId);
            CleanBlogEntryId = cleanBlogEntryId;
        }
        private void IsBlogEntryIdValid(int cleanBlogEntryId)
        {
            IntegerValidator validator = new IntegerValidator(0, Int32.MaxValue);
            validator.Validate(cleanBlogEntryId);
        }
    }
}
