﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Web;

namespace WebGoat.NET.ModelsPrimitives
{
    public class QuantityPrimitive
    {
        public short Quantity { get; private set; }

        public QuantityPrimitive(short quantity)
        {
            IsQuantityValid(quantity);
            Quantity = quantity;
        }

        private void IsQuantityValid(short quantity)
        {
            if(quantity <= 0 || quantity > 200)
            {
                throw new ArgumentException("Quantity must be between 1 and 200");
            }
        }

    }
}
