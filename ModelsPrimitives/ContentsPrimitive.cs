﻿using System.Configuration;
using System.Web;

namespace WebGoat.NET.ModelsPrimitives
{
    public class ContentsPrimitive
    {
        public string CleanContents { get; private set; }
        public ContentsPrimitive(string cleanContents)
        {
            IsContentValid(cleanContents);
            CleanContents = HttpUtility.HtmlEncode(cleanContents); //Sanitize input for XSS
        }
        private void IsContentValid(string contents)
        {
            //setup validation requirements, min and max length
            StringValidator validator = new StringValidator(1, 256);
            validator.Validate(contents); //check length
        }
    }
}
